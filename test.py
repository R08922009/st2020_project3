# -*- coding: utf-8 -*
import os
import time
#os.system('adb shell input swipe 200 1000 200 200')
#time.sleep(0.5)
#os.system('adb shell getevent | grep -e "0035" -e "0036"')
# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')
#1. [Content] Side Bar Text
def testcase1():
        time.sleep(2)
        os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
        f = open('window_dump.xml', 'r', encoding="utf-8")
        xmlString = f.read()
        assert xmlString.find('查看商品分類') != -1
        assert xmlString.find('查訂單/退訂退款') != -1
        assert xmlString.find('追蹤/買過/看過清單') != -1
        assert xmlString.find('智慧標籤')!=-1
        assert xmlString.find('其他')!=-1
        assert xmlString.find('PChome 旅遊')!=-1
        assert xmlString.find('線上手機回收')!=-1
        assert xmlString.find('給24h購物APP評分') != -1


# 2. [Screenshot] Side Bar Text
def test_case2():
        os.system('adb shell screencap -p /sdcard/case2_side_bar_text.png')
        os.system('adb pull /sdcard/case2_side_bar_text.png')
# 3. [Context] Categories
def test_case3():
        os.system('adb shell input tap 1000 200')
        time.sleep(1.5)
        os.system('adb shell input swipe 200 1000 200 200')
        time.sleep(0.5)
        os.system('adb shell input tap 1001 381')
        time.sleep(2)
        os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
        f = open('window_dump.xml', 'r', encoding="utf-8")
        xmlString = f.read()
        assert xmlString.find('精選') != -1
        assert xmlString.find('3C') != -1
        assert xmlString.find('周邊') != -1
        assert xmlString.find('NB') != -1
        assert xmlString.find('通訊') != -1
        assert xmlString.find('數位') != -1
        assert xmlString.find('家電') != -1
        assert xmlString.find('日用') != -1
        assert xmlString.find('食品') != -1
        assert xmlString.find('生活') != -1
        assert xmlString.find('運動戶外') != -1
        assert xmlString.find('美妝') != -1
        assert xmlString.find('衣鞋包錶') != -1


# 4. [Screenshot] Categories
def test_case4():
        time.sleep(0.5)
        os.system('adb shell screencap -p /sdcard/case4_categories.png')
        os.system('adb pull /sdcard/case4_categories.png')

# 5. [Context] Categories page
def test_case5():
        os.system('adb shell input tap 313 1716')
        time.sleep(1.5)
        os.system('adb shell input tap 313 1716')
        time.sleep(1.5)
        os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
        f = open('window_dump.xml', 'r', encoding="utf-8")
        xmlString = f.read()
        assert xmlString.find('24H購物') != -1

# 6. [Screenshot] Categories page
def test_case6():
        os.system('adb shell screencap -p /sdcard/case6_categories_page.png')
        os.system('adb pull /sdcard/case6_categories_page.png')

# 7. [Behavior] Search item “switch”
def test_case7():
        os.system('adb shell input tap 469 117')
        time.sleep(1)
        os.system('adb shell input text "switch"')
        os.system('adb shell screencap -p /sdcard/case7_search.png')
        os.system('adb pull /sdcard/case7_search.png')
        os.system('adb shell input keyevent "KEYCODE_ENTER"')
# 8. [Behavior] Follow an item and it should be add to the list
def test_case8():
        #click the item
        time.sleep(3)
        os.system('adb shell input tap 211 1029')
        #clisk follow
        time.sleep(8)
        os.system('adb shell input tap 114 1716')
        #go back to search page
        time.sleep(3)
        os.system('adb shell input tap 73 123')
        #go back to home page
        time.sleep(3)
        os.system('adb shell input tap 100 1713')
        #watch list
        time.sleep(3)
        os.system('adb shell input tap 70 138')
        time.sleep(5)
        os.system('adb shell input tap 469 874')
        time.sleep(15)
        os.system('adb shell screencap -p /sdcard/case8_follow_item.png')
        os.system('adb pull /sdcard/case8_follow_item.png')

# 9. [Behavior] Navigate tto the detail of item
def test_case9():
        time.sleep(0.5)
        os.system('adb shell input tap 134 722')
        time.sleep(8)
        os.system('adb shell input swipe 595 1522 595 551')
        time.sleep(1.5)
        os.system('adb shell input tap 551 126')
        time.sleep(8)
        os.system('adb shell screencap -p /sdcard/case9_detail.png')
        os.system('adb pull /sdcard/case9_detail.png')

# 10. [Screenshot] Disconnetion Screen
def test_case10():
        time.sleep(1)
        os.system('adb shell svc data disable')
        os.system('adb shell svc wifi disable')
        time.sleep(1)
        os.system('adb shell screencap -p /sdcard/case10_disconnect.png')
        os.system('adb pull /sdcard/case10_disconnect.png')



